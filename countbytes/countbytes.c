#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <err.h>

#include <stdio.h>
#include <stdlib.h>

#include <dirent.h>

#include <errno.h>

#define BLOCK_SIZE 4096
#define FORKS 4

int streq(char *a, char *b) {
	for(int i = 0;; i += 1) {
		if (a[i] != b[i]) return 0;
		if (a[i] == 0) return 1;
	}
}

int count_bytes(int fd, int *count, off_t seek_offset) {
	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(fd, block, sizeof block);
		if (bytes_read == -1) return -1;

		if (seek_offset) {
			if (lseek(fd, seek_offset, SEEK_CUR) == -1) return -1;
		}

		for (int i = 0; i < bytes_read; i += 1) {
			count[block[i]] += 1;
		}
	} while (bytes_read);

	return 0;
}

int count_bytes_forked(int ifd, int ofd, int fork_idx) {
	// Assuming there are 3 forks, blocks are to be read like so:
	//
	// 012012012012...
	//
	// If we cannot set the initial offset for reading, we assume the file descriptor is
	// unseekable. Thus we terminate all but the 0th fork and make it read like so:
	//
	// 000000000000...

	int seek_offset = (FORKS - 1) * BLOCK_SIZE;

	if (lseek(ifd, fork_idx * BLOCK_SIZE, SEEK_SET) == -1) {
		if (fork_idx != 0) return 0;
		seek_offset = 0;
	}

	int count[256] = {};
	if (count_bytes(ifd, count, seek_offset) == -1) return -1;
	if (write(ofd, count, sizeof count)      == -1) return -1;

	return 0;
}

int close_all_fds_except_0_1_2_and_arg(int fd) {
	DIR *dir = opendir("/proc/self/fd/");
	if (!dir) return -1;

	errno = 0;
	for (struct dirent *it = readdir(dir); it; it = readdir(dir)) {
		if (streq(it->d_name, ".")) continue;
		if (streq(it->d_name, "..")) continue;

		int cur_fd = atoi(it->d_name);

		if (cur_fd == dirfd(dir)) continue;

		if (cur_fd == 0) continue;
		if (cur_fd == 1) continue;
		if (cur_fd == 2) continue;

		if (cur_fd == fd) continue;

		if (close(fd) == -1) return -1;

	}
	if (errno) return -1;

	return 0;
}

void count_bytes_forking(char *filename, int *count) {
	pid_t pids[FORKS];

	int pipes[FORKS][2];

	for (int i = 0; i < FORKS; i += 1) {
		if (pipe(pipes[i]) == -1) err(6, NULL);

		pids[i] = fork();
		if (pids[i] == -1) err(4, NULL);

		if (pids[i] == 0) {
			if (close_all_fds_except_0_1_2_and_arg(pipes[i][1]) == -1) err(13, NULL);

			int ifd = open(filename, O_RDONLY);
			if (ifd == -1) {
				close(pipes[i][1]);
				err(11, "%s", filename);
			}

			int res = count_bytes_forked(ifd, pipes[i][1], i);

			close(ifd);
			close(pipes[i][1]);

			if (res == -1) err(12, "%s", filename);

			exit(0);
		}

		close(pipes[i][1]);
	}

	for (int i = 0; i < FORKS; i += 1) {
		int wstatus;
		if (wait(&wstatus) == -1) err(5, NULL);
		if (!WIFEXITED(wstatus))  errx(10, "Child process did not terminate normally");
		if (WEXITSTATUS(wstatus)) errx(WEXITSTATUS(wstatus), "Child process did not terminate normally");
	}

	for (int i = 0; i < FORKS; i += 1) {
		int subcount[256];
		int bytes_read = read(pipes[i][0], subcount, sizeof subcount);

		close(pipes[i][0]);
		if (bytes_read == 0) continue;
		if (bytes_read != sizeof subcount) err(7, NULL);

		for (int j = 0; j < 256; j += 1) {
			count[j] += subcount[j];
		}
	}
}

int main(int argc, char *argv[]) {
	int count[256] = {};

	if (argc == 1) {
		if (count_bytes(0, count, 0) == -1) err(10, "stdin");
	} else for (int i = 1; i < argc; i += 1) {
		count_bytes_forking(argv[i], count);
	}

	for (int i = 0; i < 256; i += 1) {
		if (count[i]) printf("%02x %d\n", i, count[i]);
	}

	return 0;
}
