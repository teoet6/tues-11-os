#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#include <stdio.h>
#include <stdlib.h>

#define BLOCK_SIZE 4096

int streq(char *a, char *b) {
	for(int i = 0;; i += 1) {
		if (a[i] != b[i]) return 0;
		if (a[i] == 0) return 1;
	}
}

// read backwards
ssize_t daer(int fd, void *buf, size_t nbytes) {
	{
		int cur_pos = lseek(fd, 0, SEEK_CUR);
		if (cur_pos == -1) return -1;
		if (cur_pos < nbytes) nbytes = cur_pos;
	}

	if (nbytes == 0) return nbytes;

	if (lseek(fd, -nbytes, SEEK_CUR) == -1) return -1;
	if (nbytes != read(fd, buf, nbytes))    return -1;
	if (lseek(fd, -nbytes, SEEK_CUR) == -1) return -1;

	return nbytes;
}

int tail(int ifd, int ofd, int max_lines) {
	int cur_lines = 0;

	if (lseek(ifd, 0, SEEK_END) == -1) return -1;

	int bytes_read;

	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = daer(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		for (int i = bytes_read - 1; i >= 0; i -= 1) {
			cur_lines += block[i] == '\n';
			if (cur_lines == max_lines) {
				if (lseek(ifd, i, SEEK_CUR) == -1) return -1;
				goto end_of_reading;
			}
		}
	} while (bytes_read);
end_of_reading:

	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		if (write(ofd, block, bytes_read) != bytes_read) return -1;
	} while (bytes_read);

	return 0;
}

int main(int argc, char *argv[]) {
	char *filename = NULL;
	int lines = 10;

	{
		bool parse_optionals = true;
		for (int i = 1; i < argc; i += 1) {
			if (streq("--", argv[i])) {
				parse_optionals = false;
				continue;
			}

			if (parse_optionals && streq("-n", argv[i])) {
				i += 1;
				lines = atoi(argv[i]);
				continue;
			}

			filename = argv[i];
		}
	}

	int fd = open(filename, O_RDONLY);
	if (fd == -1) err(1, "%s", filename);

	int res = tail(fd, 1, lines);

	close(fd);

	if (res == -1) err(2, "%s", filename);

	return 0;
}
