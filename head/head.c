#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#include <stdio.h>
#include <stdlib.h>

#define BLOCK_SIZE 4096

int streq(char *a, char *b) {
	for(int i = 0;; i += 1) {
		if (a[i] != b[i]) return 0;
		if (a[i] == 0) return 1;
	}
}

int head(int ifd, int ofd, int max_lines) {
	int cur_lines = 0;

	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		for (int i = 0; i < bytes_read; i += 1) {
			cur_lines += block[i] == '\n';
			if (cur_lines == max_lines) {
				if (i + 1 != write(ofd, block, i + 1)) return -1;
				return 0;
			}
		}

		if (bytes_read != write(ofd, block, bytes_read)) return -1;
	} while (bytes_read);

	return 0;
}

int main(int argc, char *argv[]) {
	char *filename  = NULL;
	int lines = 10;

	{
		bool parse_optionals = true;
		for (int i = 1; i < argc; i += 1) {
			if (streq("--", argv[i])) {
				parse_optionals = false;
				continue;
			}

			if (parse_optionals && streq("-n", argv[i])) {
				i += 1;
				lines = atoi(argv[i]);
				continue;
			}

			filename = argv[i];
		}
	}

	int fd = open(filename, O_RDONLY);
	if (fd == -1) err(1, "%s", filename);

	int res = head(fd, 1, lines);

	close(fd);

	if (res == -1) err(2, "%s", filename);

	return 0;
}
