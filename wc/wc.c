#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#include <stdio.h>

#define BLOCK_SIZE 4096

int streq(char *a, char *b) {
	for(int i = 0;; i += 1) {
		if (a[i] != b[i]) return 0;
		if (a[i] == 0) return 1;
	}
}

int wc(int ifd, int *lines, int *chars) {
	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		for (int i = 0; i < bytes_read; i += 1) *lines += block[i] == '\n';
		*chars += bytes_read;
	} while (bytes_read);

	return 0;
}

int main(int argc, char *argv[]) {
	char *filename  = NULL;
	bool show_lines = false;
	bool show_chars = false;

	{
		bool parse_optionals = true;
		for (int i = 1; i < argc; i += 1) {
			if (streq("--", argv[i])) {
				parse_optionals = false;
				continue;
			}

			if (parse_optionals && streq("-l", argv[i])) {
				show_lines = true;
				continue;
			}

			if (parse_optionals && streq("-c", argv[i])) {
				show_chars = true;
				continue;
			}

			filename = argv[i];
		}
	}

	if (!show_lines && !show_chars) show_lines = show_chars = true;

	int lines = 0;
	int chars = 0;

	int fd = open(filename, O_RDONLY);
	if (fd == -1) err(1, "%s", filename);

	int res = wc(fd, &lines, &chars);

	close(fd);

	if (res == -1) err(2, "%s", filename);

	if (show_lines) printf("%d\n", lines);
	if (show_chars) printf("%d\n", chars);

	return 0;
}
