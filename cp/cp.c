#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#define BLOCK_SIZE 4096

int cp(int ifd, int ofd) {
	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		int bytes_written = write(ofd, block, bytes_read);
		if (bytes_written != bytes_read) return -1;
	} while (bytes_read);

	return 0;
}

int main(int argc, char *argv[]) {
	if (argc != 3) errx(1, "usage: %s src dst", argv[0]);

	int ifd = open(argv[1], O_RDONLY);
	if (ifd == -1) {
		err(2, "%s", argv[1]);
	}

	int ofd = creat(argv[2], 00644);
	if (ofd == -1) {
		close(ifd);
		err(3, "%s", argv[2]);
	}

	int res = cp(ifd, ofd);

	close(ifd);
	close(ofd);

	if (res) err(4, NULL);

	return 0;
}
