#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#define BLOCK_SIZE 4096

int cat(int ifd, int ofd) {
	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(ifd, block, sizeof block);
		if (bytes_read == -1) return -1;

		int bytes_written = write(ofd, block, bytes_read);
		if (bytes_written != bytes_read) return -1;
	} while (bytes_read);

	return 0;
}

int main(int argc, char *argv[]) {
	if (argc == 1) {
		int res = cat(0, 1);
		if (res == -1) err(2, NULL);
	} else for (int i = 1; i < argc; i += 1) {
		int fd = open(argv[i], O_RDONLY);
		if (fd == -1) err(1, "%s", argv[i]);

		int res = cat(fd, 1);

		close(fd);

		if (res == -1) err(2, "%s", argv[i]);
	}

	return 0;
}
