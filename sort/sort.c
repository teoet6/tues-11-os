#include <unistd.h>
#include <fcntl.h>
#include <err.h>

#define BLOCK_SIZE 4096

int do_count(int fd, int cnt[256]) {
	int bytes_read;
	do {
		unsigned char block[BLOCK_SIZE];

		bytes_read = read(fd, block, sizeof block);
		if (bytes_read == -1) return -1;

		for (int i = 0; i < bytes_read; i += 1) {
			cnt[block[i]] += 1;
		}
	} while (bytes_read);

	return 0;
}

int write_count(int fd, int cnt[256]) {
	int block_len = 0;
	unsigned char block[BLOCK_SIZE];

	for (int byte = 0; byte < 256; byte += 1) {
		for (int i = 0; i < cnt[byte]; i += 1) {
			block[block_len] = byte;
			block_len += 1;

			if (block_len == BLOCK_SIZE) {
				if (write(fd, block, block_len) != block_len) return -1;
				block_len = 0;
			}
		}
	}

	if (block_len) {
		if (write(fd, block, block_len) != block_len) return -1;
	}

	return 0;
}

int main(int argc, char *argv[]) {
	if (argc != 2) errx(1, "usage: %s file", argv[0]);

	int fd = open(argv[1], O_RDWR);
	if (fd == -1) err(2, "%s", argv[1]);

	int cnt[256] = {};

	if (do_count(fd, cnt)      == -1) err(3, "%s", argv[1]);
	if (lseek(fd, 0, SEEK_SET) == -1) err(4, "%s", argv[1]);
	if (write_count(fd, cnt)   == -1) err(5, "%s", argv[1]);

	close(fd);

	return 0;
}
